# [CSGO] Tactical Shield
This plugin adds a tactical shield to the game. It is designed to improve the tactical side of the game.

You can use this plugin along with my other plugin [Cameras and Drones](https://gitlab.com/Keplyx/cameras-and-drones).

## Watch the [video!](https://www.youtube.com/watch?v=3LSul0V9eTo)

## See the [wiki](https://gitlab.com/Keplyx/TacticalShield/wikis/home) for more information

![screenshot_20171203_110630](https://user-images.githubusercontent.com/23726131/33524482-5a4a16d8-d81e-11e7-9772-2ca653728f40.png)

## Installation

Simply download **[tacticalshield.smx](https://gitlab.com/Keplyx/TacticalShield/raw/master/csgo/addons/sourcemod/plugins/tacticalshield.smx)** and place it in your server inside "csgo/addons/sourcemod/plugins/".
More information [here](https://gitlab.com/Keplyx/TacticalShield/wikis/Installation).


## [Cvars](https://gitlab.com/Keplyx/TacticalShield/raw/master/csgo/cfg/sourcemod/tacticalshield.cfg)

## [Commands](https://gitlab.com/Keplyx/TacticalShield/wikis/commands)

## [Changelog](https://gitlab.com/Keplyx/TacticalShield/blob/master/CHANGELOG.md)

### Creator: [Keplyx](https://gitlab.com/Keplyx)
